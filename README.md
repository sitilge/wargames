# wargames

Notes on [wargames](https://overthewire.org/wargames).

## Notes

### Bandit

- `xxd` for binary files. Use `-r` flag for reverse (hex to binary).
- `base64` for base64 files. Use `-d` flag for decode.
- `alias rot13="tr '[A-Za-z]' '[N-ZA-Mn-za-m]'"` alias for rot13 substitution cipher.
- `zcat` for expanding compressed files.
- `ssh-keygen -e -f <file>` for generating a public/private key using a given public/private key.
- `telnet` for connecting to local via TELNET protocol.
- https://en.wikipedia.org/wiki/Transport_Layer_Security
- https://www.feistyduck.com/library/openssl-cookbook/online/ch-testing-with-openssl.html
- `openssl s_client -connect sitilge.id.lv` for connecting to a server.
- `nmap -p 31000-32000 localhost --script ssl-enum-ciphers` for scanning ports 31000 to 32000 and checking if they speak SSL.
- `ssh -T` for disabling pseudo-terminal allocation.
- `nc` for arbitrary TCP and UDP connections and listens.
- It is much faster to write differenct combinations to file and then read from `nc` than calling `nc` in each iteration.

````
#!/bin/sh

for i in 0 1 2 3 4 5 6 7 8 9; do
	for j in 0 1 2 3 4 5 6 7 8 9; do
		for k in 0 1 2 3 4 5 6 7 8 9; do
			for l in 0 1 2 3 4 5 6 7 8 9; do
				combination=${i}${j}${k}${l}
				echo "UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ ${combination}"
			done
		done
	done
done
````

### Natas

- Natas 13 -> Natas 14 is pretty nice. Play around with `xxd` and `./Natas/natas13.*` to get more comfortable with hexdumps. Note: the first 4 bytes of `natas13.jpg` are `.jpg` file signatures.
- Natas 15 -> Natas 16 requires blind injection.
